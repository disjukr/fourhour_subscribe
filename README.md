Fourhour_subscribe api reference
================================


Method
------

You can call api functions defined by fourhour_subscribe player.
In following examples, `fourhour` is the id of player dom element.


### **setAddress** ###

#### parameter ####
value: address string
#### example ####
    fourhour.setAddress('127.0.0.1');


### **setChannel** ###

#### parameter ####
value: channel string
#### example ####
    fourhour.setChannel('channel_name');


### **setRoom** ###

#### parameter ####
value: room string
#### example ####
    fourhour.setChannel('room_name');


### **getVolume** ###

returns [0-100] int value

#### parameter ####
none
#### example ####
    fourhour.getVolume();


### **setVolume** ###

#### parameter ####
value: volume int value [0-100]
#### example ####
    fourhour.setVolume(50);


### **startSubscribe** ###

same with press play button ui

#### parameter ####
none
#### example ####
    fourhour.startSubscribe();


### **stopSubscribe** ###

same with press pause button ui

#### parameter ####
none
#### example ####
    fourhour.startSubscribe();


### **getConnected** ###

returns bool value

#### parameter ####
none
#### example ####
    fourhour.getVolume();



Callback
--------

Fourhour_subscribe player calls javascript's function
when specific event occured


### **onInit** ###

called when fourhour api is readied

#### example ####
    function onInit() {
        console.log('api initialized');
    }


### **onConnect** ###

called when connection succeed

#### parameter ####
debugMsg: debug message string
#### example ####
    function onConnect() {
        console.log(debugMsg);
    }


### **onError** ###

called when connection failed

#### parameter ####
debugMsg: debug message string
#### example ####
    function onError() {
        console.log(debugMsg);
    }


### **onDisconnect** ###

called when disconnected(even called when onError occured)

#### example ####
    function onDisconnect() {
        console.log('disconnected');
    }
package;

import flash.display.Stage;
import flash.display.StageAlign;
import flash.display.StageDisplayState;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.FullScreenEvent;
import flash.events.NetStatusEvent;
import flash.external.ExternalInterface;
import flash.Lib;
import flash.media.Video;
import flash.net.NetConnection;
import flash.net.NetStream;
import flash.system.Security;

class Main
{
	var stage:Stage;
	
	var connection:NetConnection;
	var stream:NetStream;
	
	var address:String;
	var channel:String;
	var room:String;
	
	var display:Video;
	var ui:FourHourUI;
	
	function new()
	{
		Security.allowDomain("*");
		stage = Lib.current.stage;
		stage.scaleMode = StageScaleMode.NO_SCALE;
		stage.align = StageAlign.TOP_LEFT;
		
		stage.addEventListener(Event.RESIZE, RESIZE);
		stage.addEventListener(Event.FULLSCREEN, FULLSCREEN);
		
		display = cast stage.addChild(new Video(stage.stageWidth, stage.stageHeight - 40));
		ui = cast stage.addChild(new FourHourUI());
		ui.playCallback = function (value:Bool):Void {
			if (value) startSubscribe() else stopSubscribe();
		};
		ui.volumeCallback = function (value:Float):Void {
			setVolume(cast value * 100);
		};
		ui.fullCallback = function (value:Bool):Void {
			stage.displayState = if (value)
				StageDisplayState.FULL_SCREEN;
			else
				StageDisplayState.NORMAL;
		};
		
		if (ExternalInterface.available)
		{
			ExternalInterface.addCallback("setAddress", setAddress);
			ExternalInterface.addCallback("setChannel", setChannel);
			ExternalInterface.addCallback("setRoom", setRoom);
			ExternalInterface.addCallback("getVolume", getVolume);
			ExternalInterface.addCallback("setVolume", setVolume);
			ExternalInterface.addCallback("startSubscribe", startSubscribe);
			ExternalInterface.addCallback("stopSubscribe", stopSubscribe);
			ExternalInterface.addCallback("getConnected", getConnected);
			ExternalInterface.call("onInit");
		}
	}
	
	function RESIZE(e:Event)
	{
		display.width = stage.stageWidth;
		display.height = stage.stageHeight - 40;
	}
	
	function FULLSCREEN(e:FullScreenEvent)
	{
		ui.updateFullStatus(e.fullScreen, false);
	}
	
	function NET_STATUS(e:NetStatusEvent)
	{
		function jsEvent(name:String)
		{
			if (ExternalInterface.available)
				ExternalInterface.call(name, e.info.code);
		}
		if (e.info.level == "error")
		{
			jsEvent("onError");
			stopSubscribe();
		}
		else switch (e.info.code)
		{
			case "NetConnection.Connect.Success":
				stream = new NetStream(connection);
				display.attachNetStream(stream);
				stream.play(room);
				jsEvent("onConnect");
			case "NetConnection.Connect.Closed":
				jsEvent("onError");
				stopSubscribe();
		}
	}
	
	static function main()
	{
		new Main();
	}
	
	function setAddress(value)
	{
		address = value;
	}
	
	function setChannel(value)
	{
		channel = value;
	}
	
	function setRoom(value)
	{
		room = value;
	}
	
	function getVolume()
	{
		return if (cast stream) stream.soundTransform.volume * 100 else 100;
	}
	
	function setVolume(value)
	{
		if (cast stream)
		{
			var soundTransform = stream.soundTransform;
			soundTransform.volume = value * 0.01;
			stream.soundTransform = soundTransform;
		}
		ui.updateVolumeStatus(value * 0.01, false);
	}
	
	function startSubscribe()
	{
		if (cast connection) connection.removeEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
		connection = new NetConnection();
		connection.addEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
		connection.connect("rtmp://" + address + "/" + channel);
		ui.updatePlayStatus(true, false);
	}
	
	function stopSubscribe()
	{
		if (cast connection) connection.removeEventListener(NetStatusEvent.NET_STATUS, NET_STATUS);
		if (getConnected()) connection.close();
		if (cast stream) stream.close();
		connection = null;
		stream = null;
		if (ExternalInterface.available)
				ExternalInterface.call("onDisconnect");
		ui.updatePlayStatus(false, false);
	}
	
	function getConnected()
	{
		return (cast connection) && (cast connection.connected);
	}
}
package;

import flash.display.MovieClip;

extern class FourHourUI extends MovieClip
{
	var fullCallback:Bool->Void;
	var playCallback:Bool->Void;
	var volumeCallback:Float->Void;

	function new():Void;

	function updateFullStatus(value:Bool, ?callback:Bool):Void;
	function updatePlayStatus(value:Bool, ?callback:Bool):Void;
	function updateVolumeStatus(value:Float, ?callback:Bool):Void;
}
﻿package 
{
	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	public class FourHourUI extends MovieClip
	{
		private var playStatus:Boolean;
		private var volumeStatus:Number;
		private var fullStatus:Boolean;
		
		private var play_bt:MovieClip;
		private var mute_bt:MovieClip;
		private var volume_slider:MovieClip;
		private var full_bt:MovieClip;
		private var logo:MovieClip;
		
		public var playCallback:Function;
		public var volumeCallback:Function;
		public var fullCallback:Function;
		
		public function FourHourUI()
		{
			addEventListener(Event.ADDED_TO_STAGE, ADDED_TO_STAGE);
		}
		
		private function ADDED_TO_STAGE(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, ADDED_TO_STAGE);
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			play_bt = addChild(new _play_bt) as MovieClip;
			mute_bt = addChild(new _mute_bt) as MovieClip;
			volume_slider = addChild(new _volume_slider) as MovieClip;
			full_bt = addChild(new _full_bt) as MovieClip;
			logo = addChild(new _logo) as MovieClip;
			play_bt.x = 0;
			mute_bt.x = 40;
			volume_slider.x = 90;
			full_bt.x = 180;
			play_bt.stop();
			mute_bt.stop();
			full_bt.stop();
			volume_slider.volume_bt.buttonMode =
				full_bt.buttonMode =
				mute_bt.buttonMode =
				play_bt.buttonMode = true;
			updatePlayStatus(false);
			updateVolumeStatus(1);
			updateFullStatus(false);
			
			playCallback = function (playStatus) {
				trace(playStatus? "now playing" : "paused");
			};
			volumeCallback = function (volumeStatus) {
				trace("volume: " + (volumeStatus * 100) + "%");
			};
			fullCallback = function (fullStatus) {
				trace(fullStatus? "now fullscreen" : "windowed");
			};
			play_bt.addEventListener(MouseEvent.MOUSE_DOWN,
				function (e:MouseEvent):void {
					updatePlayStatus(!playStatus);
				});
			
			mute_bt.addEventListener(MouseEvent.MOUSE_DOWN,
				function (e:MouseEvent):void {
					updateVolumeStatus(volumeStatus == 0? 1 : 0);
				});
			
			full_bt.addEventListener(MouseEvent.MOUSE_DOWN,
				function (e:MouseEvent):void {
					updateFullStatus(!fullStatus);
				});
			
			stage.addEventListener(Event.RESIZE,
				function (e:Event):Function {
					var sw:int = stage.stageWidth;
					var sh:int = stage.stageHeight;
					with (graphics) {
						clear();
						beginFill(0x000000);
						drawRect(0, sh - 40, sw, 40);
					}
					logo.x = sw;
					play_bt.y = mute_bt.y = full_bt.y = logo.y = sh;
					volume_slider.y = sh - 20;
					return arguments.callee;
				}(null));
		}
		
		public function updateVolumeStatus(value:Number, callback:Boolean = true):void
		{
			volumeStatus = value;
			volume_slider.renderBar(value);
			volume_slider.volume_bt.x = value * 80;
			if (volumeStatus == 0)
				mute_bt.gotoAndStop(2);
			else
				mute_bt.gotoAndStop(1);
			if (volumeCallback != null) if (callback) volumeCallback(volumeStatus);
		}
		
		public function updatePlayStatus(value:Boolean, callback:Boolean = true):void
		{
			playStatus = value;
			if (value)
				play_bt.gotoAndStop(2);
			else
				play_bt.gotoAndStop(1);
			if (playCallback != null) if (callback) playCallback(playStatus);
		}
		
		public function updateFullStatus(value:Boolean, callback:Boolean = true):void
		{
			fullStatus = value;
			if (value)
				full_bt.gotoAndStop(2);
			else
				full_bt.gotoAndStop(1);
			if (fullCallback != null) if (callback) fullCallback(fullStatus);
		}

	}
}